package ru.experiments.benchmark

import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import ru.experiments.benchmark.utils.SynchronizedLazyNoSave

/**
 * Сравнение чтений из синхронизованных/НЕсинхронизованных lazy переменных
 *
 * Результаты
 * Sony XperiaX1 (Android 9):
 *             6 ns   LazyBenchmark.1___PlainRead
 *             12 ns  LazyBenchmark.2_1_LazyUnsafeFirstRead
 *             48 ns  LazyBenchmark.2_2_LazyFirstRead
 *             210 ns LazyBenchmark.2_3_LazyFirstRead_SpoiledByHashCode
 *             15 ns  LazyBenchmark.3_1_LazyNextRead
 *             12 ns  LazyBenchmark.3_2_LazyUnsafeNextRead
 *
 * SamsungA52 (Android 12)
 *             4 ns   LazyBenchmark.1___PlainRead
 *             6 ns   LazyBenchmark.2_1_LazyUnsafeFirstRead
 *             33 ns  LazyBenchmark.2_2_LazyFirstRead
 *             113 ns LazyBenchmark.2_3_LazyFirstRead_SpoiledByHashCode
 *             6 ns   LazyBenchmark.3_1_LazyNextRead
 *             6 ns   LazyBenchmark.3_2_LazyUnsafeNextRead
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class LazyBenchmark {

    @get:Rule
    val benchmarkRule = BenchmarkRule()

    /**
     * Чтение из локала, удобно использовать как baseline
     */
    @Test
    fun test1___PlainRead() {
        val plainValue = 1000
        benchmarkRule.measureRepeated { plainValue }
    }

    /**
     * Первое чтение из НЕсинхронизованного lazy
     */
    @Test
    fun test2_1_LazyUnsafeFirstRead() {
        val lazyValue = lazy(LazyThreadSafetyMode.NONE) { 1000 }
        benchmarkRule.measureRepeated { lazyValue.value }
    }

    /**
     * Первое чтение из синхронизованного lazy.
     * Т.к. методика измерений такова, что код под "measureRepeated{}" гоняется много раз,
     * пришлось сделать класс [SynchronizedLazyNoSave], чтобы каждый раз вычислять значение как в первый раз
     * @see SynchronizedLazyNoSave
     */
    @Test
    fun test2_2_LazyFirstRead() {
        val lazyValue = SynchronizedLazyNoSave { 1000 }
        benchmarkRule.measureRepeated { lazyValue.value }
    }

    /**
     * Первое чтение из синхронизованного lazy, если до чтения не нем вычислили HashCode.
     * Данный тест показывает время работы lazy, в случае если приходится надувать тяжелый монитор,
     * подробности можно прочитать в комментарии https://android.googlesource.com/platform/art/+/master/runtime/monitor.cc#57
     * @see SynchronizedLazyNoSave
     */
    @Test
    fun test2_3_LazyFirstRead_SpoiledByHashCode() {
        val lazyValue = SynchronizedLazyNoSave { 1000 }
        lazyValue.hashCode()
        benchmarkRule.measureRepeated { lazyValue.value }
    }

    /**
     * Чтение на инициализированном НЕсинхронизованном lazy
     */
    @Test
    fun test3_2_LazyUnsafeNextRead() {
        val lazyValue = lazy(LazyThreadSafetyMode.NONE) { 1000 }
        lazyValue.value
        benchmarkRule.measureRepeated { lazyValue.value }
    }

    /**
     * Чтение на инициализированном синхронизованном lazy
     */
    @Test
    fun test3_1_LazyNextRead() {
        val lazyValue = lazy(LazyThreadSafetyMode.SYNCHRONIZED) { 1000 }
        lazyValue.value
        benchmarkRule.measureRepeated { lazyValue.value }
    }

}
