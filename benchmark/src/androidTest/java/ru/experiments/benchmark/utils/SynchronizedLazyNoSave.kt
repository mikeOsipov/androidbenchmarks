package ru.experiments.benchmark.utils

import java.io.Serializable

/**
 * Копия SynchronizedLazyImpl без сохранения значения, используется для тестирования первого чтения
 */
internal class SynchronizedLazyNoSave<out T>(lock: Any? = null, initializer: () -> T) : Lazy<T>,
    Serializable {
    private var initializer: (() -> T)? = initializer

    @Volatile
    private var _value: Any? = UNINITIALIZED_VALUE

    // final field is required to enable safe publication of constructed instance
    private val lock = lock ?: this

    override val value: T
        get() {
            val _v1 = _value
            if (_v1 !== UNINITIALIZED_VALUE) {
                @Suppress("UNCHECKED_CAST")
                return _v1 as T
            }

            return synchronized(lock) {
                val _v2 = _value
                if (_v2 !== UNINITIALIZED_VALUE) {
                    @Suppress("UNCHECKED_CAST") (_v2 as T)
                } else {
                    val typedValue = initializer!!()
                    /* Выключено сохранение значения, чтобы постоянно заходить в synchronized при попытке полуить значение
                    _value = typedValue
                    initializer = null*/
                    typedValue
                }
            }
        }

    override fun isInitialized(): Boolean = _value !== UNINITIALIZED_VALUE

    override fun toString(): String =
        if (isInitialized()) value.toString() else "Lazy value not initialized yet."

    private fun writeReplace(): Any = InitializedLazyImpl(value)
}

private object UNINITIALIZED_VALUE

private class InitializedLazyImpl<out T>(override val value: T) : Lazy<T>, Serializable {

    override fun isInitialized(): Boolean = true

    override fun toString(): String = value.toString()

}